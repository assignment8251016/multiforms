
const state = {
    page: 1,
    step2: {
        planName: "",
        plantype: "monthly",
        cost: 0
    },
    step3: {
        planName: [],
        cost: 0
    },
    checkout: false,
    filled: [0, 0, 0]
}

function sum(arr) {
    let temp = 0;
    arr.forEach(ele => {
        temp += ele;
    })

    return temp;
}
//setting logic for text boxes on page 1
const textboxes = document.querySelectorAll("#page-1-form > input[type='text']")

// adds id to html elements
function addID(tag, idName) {
    tag.setAttribute("id", idName);
}

function removeId(tag) {
    tag.removeAttribute("id");
}


function checkFilledTextbox() {
    Array.from(textboxes).forEach((textbox, index) => {
        if (textbox.value !== "") {
            state.filled[index] = 1
        }
    })
}

//buttons
const nextButtonDesktop = document.querySelector("#next-button-desktop");
const nextButtonMobile = document.querySelector("#next-button-mobile");
const nextButtons = [nextButtonDesktop, nextButtonMobile];

const backButtonDesktop = document.querySelector("#back-button-desktop");
const backButtonMobile = document.querySelector("#back-button-mobile");
const backButtons = [backButtonDesktop, backButtonMobile];

const toggleSwitch = document.querySelector("#switch");
const toggleContainer = document.querySelector("#toggle-button-2");

//card on page 2
const page2cards = document.querySelectorAll(".card-1")

//radio buttons on page 2
const page2radio = document.querySelectorAll("input[type=radio]");

//page 4 elements
const page4section1 = document.querySelector(".section-1");

const page4section2 = document.querySelector(".section-2");

const page4total = document.querySelector(".total")
//checkboxes of page 3
const page3verticalCards = document.querySelectorAll(".card-2");

//first page messages
const messages = document.querySelectorAll(".messages");

const pagetext = [document.querySelector("#page-1-text")
    , document.querySelector("#page-2-text")
    , document.querySelector("#page-3-text")
    , document.querySelector("#page-4-text")
    , document.querySelector("#page-5-text")
]

const pageform = [document.querySelector("#page-1-form")
    , document.querySelector("#page-2-form")
    , document.querySelector("#page-3-form")
    , document.querySelector("#page-4-form")
]


//event on page 2
Array.from(page2radio).forEach((radiobutton, index) => {

    // if(radiobutton.checked){
    //     page2cards[index].addEventListener("click", ()=>{
    //         page2cards[index].
    //     })
    //     console.log(radiobutton.checked)
    // }
    // else{

    //     page2cards[index].style.display = "hsl(0, 0, 100%);"
    // }
})

// toggle switch button
toggleSwitch.addEventListener("click", () => {
    Array.from(page3verticalCards).forEach(card => {
        card.querySelector("input[type=checkbox]").checked = false;
        card.style.backgroundColor = "rgb(255, 255, 255)"
    })
    if (toggleSwitch.checked) {
        //unhiding 2 months free
        Array.from(document.querySelectorAll(".time")).forEach(time => time.style.display = "block")

        Array.from(document.querySelectorAll("input[type='radio'] + .card-1>.card-info>.price-3")).forEach(price => price.style.display = "block");
        Array.from(document.querySelectorAll("input[type='radio'] + .card-1>.card-info>.price-1")).forEach(price => price.style.display = "none");

        // unhiding first 3 cards on page 3
        Array.from(document.querySelectorAll(".card-2:nth-child(n+4)")).forEach(card => card.style.display = "flex")

        // hiding first 3 cards on page 3
        Array.from(document.querySelectorAll(".card-2:nth-child(-n+3)")).forEach(card => card.style.display = "none")
    } else {
        //hiding 2 months free
        Array.from(document.querySelectorAll(".time")).forEach(time => time.style.display = "none")

        Array.from(document.querySelectorAll("input[type='radio'] + .card-1>.card-info>.price-3")).forEach(price => price.style.display = "none");
        Array.from(document.querySelectorAll("input[type='radio'] + .card-1>.card-info>.price-1")).forEach(price => price.style.display = "block");

        // hiding last 3 cards on page 3
        Array.from(document.querySelectorAll(".card-2:nth-child(n+4)")).forEach(card => card.style.display = "none")

        // unhiding first 3 cards on page 3
        Array.from(document.querySelectorAll(".card-2:nth-child(-n+3)")).forEach(card => card.style.display = "flex")
    }
})


// event handling on next button to move forward 
nextButtons.forEach((button) => button.addEventListener("click", (event) => {

    // back button visibilty 
    console.log("nextbutton", state.page);

    // logic for filled textbox.
    checkFilledTextbox();

    Array.from(messages).forEach((message, index) => {
        if (state.filled[index] === 1)
            message.style.display = "none";
        else
            message.style.display = "block";
    })

    // if all textbox is filled sum will be 3 means can move to page 2
    if (sum(state.filled) === 3) {
        if (state.page < 5) {
            state.page += 1;
        }
        backButtonDesktop.style.display = "block";
        backButtonMobile.style.display = "block";
        hidePage(state.page);
        unhidePage(state.page);

    }


    if (state.page === 2) {
        toggleContainer.style.display = "flex"
    }
    else {
        toggleContainer.style.display = "none"
    }


    // event handling on cards on page 2 if toggled
    toggleUI(toggleSwitch);


    //save state of checked cards on page 3 
    page3CheckedCards(page3verticalCards);

    if (state.page > 3) {
        // remove nodes on page 4 from section-1 div
        while (page4section1.firstElementChild) {
            page4section1.removeChild(page4section1.firstElementChild);
        }
        //appending to section-1 div on page-4
        const htmlstring = `<div class="plan-container"><div class="plan">${state.step2.planName.name} (${state.step2.plantype})</div><div id="change">change</div></div>`
        page4section1.appendChild(htmlToElement(htmlstring))
        page4section1.appendChild(htmlToElement(`<div class="main cost">+${state.step2.planName.cost}</div>`))

        //event handling on change tag 
        console.log(document.querySelector("#change"));
        document.querySelector("#change").addEventListener("click", () => {
            change();
            state.page = 2;
        })


        //remove nodes on page 4 from section-2 div
        while (page4section2.firstElementChild) {
            page4section2.removeChild(page4section2.firstElementChild);
        }
        //populating section-2 div on page 4
        state.step3.planName.forEach(name => {
            page4section2.appendChild(htmlToElement(`<div><p>${name.name}</p><p>${name.price}</p></div>`))
        })

        //populating total div on page 4
        if(toggleSwitch.checked){
            page4total.childNodes[1].innerText = `+$${parseInt(state.step2.cost) + parseInt(state.step3.cost)}/yr`
        }else{
            page4total.childNodes[1].innerText = `+$${parseInt(state.step2.cost) + parseInt(state.step3.cost)}/mo`
        }        

        nextButtons.forEach(button => button.innerText = "confirm")
    }

})
);


backButtons.forEach(button => button.addEventListener("click", () => {
    if(state.page<=2){
        state.filled = [0,0,0];
    }
    console.log("b",state.page)
    if (state.page > 2) {
        backButtonDesktop.style.display = "block";
        backButtonMobile.style.display = "block";

    } else {
        backButtonDesktop.style.display = "none";
        backButtonMobile.style.display = "none";
    }
    hidePage(state.page, true);
    unhidePage(state.page, true);
    if (state.page > 1) {
        state.page -= 1;
    }

    if (state.page === 2) {
        toggleContainer.style.display = "flex"
    }
    else {
        toggleContainer.style.display = "none"
    }

    if (state.page < 4) {
        nextButtons.forEach(button => button.innerText = "next");
    }



})
);

//event handling on vertical cards of page 3
Array.from(page3verticalCards).forEach(card => {
    card.addEventListener("click", () => {
        const tag = card.querySelector("input[type='checkbox']")

        tag.checked = !tag.checked;
        if (tag.checked) {
            card.style.backgroundColor = "hsl(225, 100%, 97%)"
            console.log("color changed!")

        } else {
            card.style.backgroundColor = "rgb(255, 255, 255)"
        }
    })
})


//saves state of all the checked vertical cards on page 3
function page3CheckedCards(cards) {
    // clearing all previous stored state
    state.step3.planName = [];
    state.step3.cost = 0
    Array.from(cards).forEach((card) => {
        const checkbox = card.querySelector("input[type='checkbox']");
        const cardname = card.querySelector(".card-2-name>p")
        const price2 = card.querySelector(".price-2");
        if (checkbox.checked) {
            state.step3.planName.push({ name: cardname.innerText, price: price2.innerText })
            state.step3.cost += parseInt(checkbox.getAttribute("value"))
            card.style.backgroundColor = "rgb(234, 240, 249);"
            // console.log(cardname.innerText)
            // console.log(checkbox.getAttribute("value"))
        }
    })
}


//logic for setting visibility of different container
function unhidePage(page, reverse = false) {



            if (reverse) {
                console.log("unhide true page", page);
                pagetext[page - 1].style.display = "none"
                pageform[page - 1].style.display = "none"
                document.querySelector(`.step:nth-child(${page})>.index`).style.backgroundColor = "transparent";
                document.querySelector(`.step:nth-child(${page})>.index`).style.color = "white";
            } else {
                console.log("unhide false page", page);
                pagetext[page - 1].style.display = "flex"
                if(page<=4){
                    pageform[page - 1].style.display = "flex"
                    document.querySelector(`.step:nth-child(${page})>.index`).style.backgroundColor = "hsl(206, 94%, 87%)";
                    document.querySelector(`.step:nth-child(${page})>.index`).style.color = "hsl(213, 96%, 18%)";
                }else{
                    nextButtons.forEach(button => button.style.display = "none");
                    backButtons.forEach(button => button.style.display = "none");
                
                }
            }

}

function hidePage(page, reverse = false) {


            if (reverse) {
                pagetext[page - 2].style.display = "flex"
                pageform[page - 2].style.display = "flex"
                console.log("hide true page", page);
                document.querySelector(`.step:nth-child(${page - 1})>.index`).style.backgroundColor = "hsl(206, 94%, 87%)";
                document.querySelector(`.step:nth-child(${page - 1})>.index`).style.color = "hsl(213, 96%, 18%)";
            } else {
                console.log("hide flase page", page);
                pagetext[page - 2].style.display = "none"
                pageform[page - 2].style.display = "none"
                document.querySelector(`.step:nth-child(${page - 1})>.index`).style.backgroundColor = "transparent";
                document.querySelector(`.step:nth-child(${page - 1})>.index`).style.color = "white";
            
                if(page>4){
                    document.querySelector(`.step:nth-child(${page - 1})>.index`).style.backgroundColor = "hsl(206, 94%, 87%)";
                    document.querySelector(`.step:nth-child(${page-1})>.index`).style.color = "hsl(213, 96%, 18%)";
                }
            }
}


//change saves card state on page 2 
// called the function on nextButton 
function toggleUI(toggleSwitch) {

    let selectedRadio = undefined;
    if (toggleSwitch.checked) {
        selectedRadio = document.querySelector("input[type='radio']:checked + .card-1>.card-info>.price-3")
        state.step2.planName = { name: document.querySelectorAll("input[type='radio']:checked + .card-1>.card-info>.card-1-name")[0].innerText, cost: selectedRadio.innerText };
        state.step2.plantype = "yearly";
        state.step2.cost = parseInt(selectedRadio.getAttribute("value"));

        //page 4 total div 
        page4total.childNodes[0].innerText = "total(per year)"

    } else {
        selectedRadio = document.querySelector("input[type='radio']:checked + .card-1>.card-info>.price-1")
        state.step2.planName = { name: document.querySelectorAll("input[type='radio']:checked + .card-1>.card-info>.card-1-name")[0].innerText, cost: selectedRadio.innerText };
        state.step2.plantype = "monthly";
        state.step2.cost = parseInt(selectedRadio.getAttribute("value"));

        //page 4 total div 
        page4total.childNodes[0].innerText = "total(per month)"
    }
}
//create html elements from string
function htmlToElement(html) {
    var template = document.createElement('template');
    html = html.trim(); // Never return a text node of whitespace as the result
    template.innerHTML = html;
    return template.content.firstChild;
}


//change button handling
function change(){

    nextButtons.forEach(button => button.innerText = "next");
    pageform.forEach((form) => {
        form.style.display = "none";
    })

    pagetext.forEach((form) => {
        form.style.display = "none";
    })

    pagetext[1].style.display = "block";
    pageform[1].style.display = "flex";

    toggleContainer.style.display = "flex";
    
    document.querySelector(`.step:nth-child(${2})>.index`).style.backgroundColor = "rgb(67, 179, 239)";
    document.querySelector(`.step:nth-child(${4})>.index`).style.backgroundColor = "transparent";
    toggleContainer.style.display = "flex";
    
}
